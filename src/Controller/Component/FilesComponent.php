<?php
namespace App\Controller\Component;

use Cake\Controller\Component;

class FileComponent extends Component{
	
	const BASE_DIR_FOR_PICTURES = "./galleryPictures/";
	
	public function getDirectories(){
		$directories = array();
		if ($handle = opendir ( self::BASE_DIR_FOR_PICTURES )) {
			while ( false !== ($entry = readdir ( $handle )) ) {
				if ($entry != "." && $entry != "..") {
					if (is_dir ( self::BASE_DIR_FOR_PICTURES . $directory . "/" . $entry ) == true) {
						$directories[] = $entry;
					}
				}
			}
			closedir ( $handle );
		}
		return $directories;
	}
	
	public function getFiles($directory){
		$files = array();
		if ($handle = opendir ( self::BASE_DIR_FOR_PICTURES . $directory )) {
			while ( false !== ($entry = readdir ( $handle )) ) {
				if ($entry != "." && $entry != "..") {
					if (is_dir ( self::BASE_DIR_FOR_PICTURES . $directory . "/" . $entry ) != true) {
						$files[] = $entry;
					}
				}
			}
			closedir ( $handle );
		}
		return $files;
	}
}