<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Fotos Controller
 *
 * @property \App\Model\Table\FotosTable $Fotos
 */
class FotosController extends AppController
{
	
	const BASE_DIR_FOR_PICTURES = "./galleryPictures/";
	
	public function beforeFilter(Event $event) {
		parent::beforeFilter($event);
		$this->Auth->allow("getGalleryItem");
		$this->Auth->allow("view");
	}
	
	public function gallery($galleryId){
		$query = $this->Fotos->findByGaleriaId($galleryId);
		$this->set('fotos',$query);
		$this->set('_serialize',['fotos']);
	}
	
	public function listDirectory() {
		$this->viewBuilder()->layout(false);

		if (array_key_exists('path', $this->request->query)) {
			$directory = $this->request->query['path'];
			$directory   = urldecode($directory);
		} else { 
			$directory = "";
		}
		
		$files_array = array();
		$fotos = $this->Fotos->find()->where(['Fotos.path LIKE' => $directory])->select(['filename','id','title']);
		$fotosData = array();
		foreach ($fotos as $foto){
			$fotosData[$foto['filename']] = ['id' => $foto['id'], 'title' => $foto['title']];
		}
		
		if ($handle = opendir ( self::BASE_DIR_FOR_PICTURES . $directory )) {
			$i = 0;
			while ( false !== ($entry = readdir ( $handle )) ) {
				if ($entry != "." && $entry != "..") {
					$included = array_key_exists ( $entry, $fotosData );
					if ($included) {
						$currentFoto = $fotosData [$entry];
					} else {
						$currentFoto = [ 
								'id' => "",
								'title' => '' 
						];
					}
					$files_array [$i] = [ 
							'file' => $entry,
							'isDir' => (is_dir ( self::BASE_DIR_FOR_PICTURES . $directory . "/" . $entry ) == true),
							'included' => $included,
							'id' => $currentFoto ['id'],
							'title' => $currentFoto ['title'] 
					];
					$i ++;
				}
			}
			closedir ( $handle );
		}
		$this->set('files',$files_array);
		$this->set('_serialize',['files']);
		$this->set('path',$directory);
	}

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Galerias']
        ];
        $fotos = $this->paginate($this->Fotos);

        $this->set(compact('fotos'));
        $this->set('_serialize', ['fotos']);
    }
    
    public function noGaleria(){
    	$fotos = $this->Fotos->find('all',[
    			'conditions' => ['galeria_id IS NULL']
    	]);
    	
    	$this->set(compact('fotos'));
    	$this->set('_serialize',['fotos']);
    }
    
    public function addToPiece($id){
    	$this->removeFromPiece($this->request->data(['piece_id']),$this->request->data(['piece_order']));
    	$foto = $this->Fotos->get($id);
    	$foto->piece_order = $this->request->data['piece_order'];
    	$foto->piece_id = $this->request->data['piece_id'];
    	$this->Fotos->save($foto);
    	$this->set('foto',$foto);
    }
    
    private function removeFromPiece($pieceId,$pieceOrder){
    	$foto = $this->Fotos->find('all',[
    		'conditions' => ['piece_id =' => $pieceId, 'piece_order =' => $pieceOrder]	
    	])->first();
    	if ($foto != null){
    		$foto->piece_id = null;
    		$foto->piece_order = null;
    		$this->Fotos->save($foto);
    		$this->set('foto',$foto);
    	}
    }

    public function view($id = null)
    {
        $foto = $this->Fotos->get($id, [
            'contain' => ['Users']
        ]);
        $basePath = "../".FotosController::BASE_DIR_FOR_PICTURES;
        
        $this->set('basePath', $basePath);
        $this->set('foto', $foto);
        $this->set('_serialize', ['foto']);
    }
    
   	public function viewByPath($path = null) {
    	if ($path != null){
    		$foto = $this->Fotos->find('all', array('conditions' => array ('Fotos.path' => $path)))->first();
    		$this->set('foto',$foto);
    	}
    	$this->viewBuilder()->layout(false);
    }
    
	public function getGalleryItem($galleryId = null, $orderNumber = null) {
    	if ($orderNumber == null || $galleryId == null) {
    		return null;
    	} else {
    		$foto = $this->Fotos->find('all', [ 'conditions' => [
    			'Fotos.galeria_id' => $galleryId], 
    			'limit' => 1, 
    			'offset' => $orderNumber
    		])->first();
    		
    		$this->set('orderNumber',$orderNumber);
    		$this->set('foto',$foto);
    		$this->set('basePath', "../" . self::BASE_DIR_FOR_PICTURES);
    	}
    }
    
    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $foto = $this->Fotos->newEntity();
        if ($this->request->is(['post']) ) {
            $foto = $this->Fotos->patchEntity($foto, $this->request->data);
            $foto->user_id = $this->Auth->user('id');
			$queryFotos = $this->Fotos->find('all',['order' => ['items_order' => 'DESC']])->first();
            $newFotoOrderNumber = $queryFotos->items_order;
            $foto->items_order = $newFotoOrderNumber + 1;
            $fotoPath = "./". self::BASE_DIR_FOR_PICTURES .$foto->path.$foto->filename;
            if ($this->Fotos->save($foto)) { 
            	$this->set('success',true);
                return $this->redirect(['action' => 'edit',$foto->id]);
            } else {
				$this->set('success',false);
            }
        }else {
	        $galerias = $this->Fotos->Galerias->find('list', ['limit' => 200]);
	        $path = $this->request->query['path'];
	        $fileName = $this->request->query['fileName'];
	        $this->set(compact('foto', 'galerias','fileName','path'));
	        $this->set('_serialize', ['foto']);
        }
    }
    
    public function delete($id = null) {
    $foto = $this->Fotos->get($id);
    	if ($this->request->is(['post'])){
    		if ($this->Fotos->delete($foto)){
    			$this->set('success',true);
    		} else {
    			$this->set('success',false);
    		}
    	}
    }

    /**
     * Edit method
     *
     * @param string|null $id Foto id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null){       
    	$foto = $this->Fotos->get($id);
        if ($this->request->is(['post'])) {
            $foto = $this->Fotos->patchEntity($foto, $this->request->data);
            $foto->user_id = $this->Auth->user('id');
            if ($this->Fotos->save($foto)) {
            	$this->set('success',true);
            } else {
                $this->set('success',false);
            }
        } else {
        	$galerias = $this->Fotos->Galerias->find('list', ['limit' => 200]);
        	$this->set(compact('foto', 'galerias','action'));
        	$this->set('_serialize', ['foto']);
        }
    }
    
    public function editOrder($id1, $id2, $newOrder1, $newOrder2){
    	if (($id1 !== null) && ($id2 !== null) && ($newOrder1 !== null) && ($newOrder2 !== null)){
    		$foto1 = $this->Fotos->get($id1);
    		$foto2 = $this->Fotos->get($id2);
    		if ($this->request->is(['post'])){
    			$foto1->items_order = $newOrder2;
    			$foto2->items_order = $newOrder1;
    			if ($this->Fotos->save($foto1) && $this->Fotos->save($foto2)){
    				$this->set('success',true);
    			} else {
    				$this->set('success',false);
    			}
    		}
    	}
    }

}
