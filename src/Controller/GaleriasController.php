<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Error\Debugger;

/**
 * Galerias Controller
 *
 * @property \App\Model\Table\GaleriasTable $Galerias
 */
class GaleriasController extends AppController
{

	const LOADED_IMAGES_MARGIN = 3;
	
	public function beforeFilter(Event $event){
		parent::beforeFilter($event);
		$this->Auth->allow('view');
	}
	
	public function index(){
		$this->redirect("/");
	}
	
	public function listAll(){
		$galerias = $this->Galerias->find('all', [
				'contain' => ['Fotos'], 'order' => ['items_order']
		]);
		$this->set('galerias',$galerias);
	}

    public function adminList(){
    	$this->viewBuilder()->layout(false);
    	$galerias = $this->paginate($this->Galerias->find('all', [
            'contain' => ['Fotos'], 'order' => ['items_order']
        ]));
    	$this->set(compact('galerias'));
    	$this->set('_serialize',['galerias']);
    }
    
    public function view($id = null, $requested_foto_id = 0)
    {
    	try {
        	$galeria = $this->Galerias->get($id, [
            	'contain' => ['Users', 
            		'Fotos' => function ($q) {
            		return $q->order(['Fotos.items_order' => 'ASC']);
            	}]
        	]);
    	} catch (RecordNotFoundException $e){        
        	return $this->redirect("/");
        }
        $offset = 0;
    	if ($requested_foto_id !== 0){
    		$i = 0;
    		foreach($galeria->fotos as $foto){
    			if ($foto->id == $requested_foto_id){
    				$offset = $i;
    			}
    			$i++;
    		}
    	}
        $basePath = "../".FotosController::BASE_DIR_FOR_PICTURES;
		$fotosLimit = self::LOADED_IMAGES_MARGIN;
		
		$this->set('fotosLimit', $fotosLimit);
        $this->set('basePath', $basePath);
        $this->set('galeria', $galeria);
        $this->set('offset', $offset);
        $this->set('_serialize', ['galeria']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $galeria = $this->Galerias->newEntity();
    	if ($this->request->is(['post'])) {
    		
            $galeria = $this->Galerias->patchEntity($galeria, $this->request->data);
            $galeria->user_id = $this->Auth->user('id');
            $queryAllGalerias = $this->Galerias->find('all');
            $galeria->items_order = $queryAllGalerias->count()+1;
            if ($this->Galerias->save($galeria)) {
            	if ($galeria->folder != null){
            		$Fotos = $this->loadModel('Fotos');
            		$Fotos->removeByGallery($galeria->id);
            		$Fotos->addByFolder($galeria);
            	}
                $this->set('success',true);
                return $this->redirect(['action' => 'edit',$galeria->id]);
            } else {
                $this->set('success',false);
            }
        } else {
        	$users = $this->Galerias->Users->find('list', ['limit' => 200]);
        	$this->set(compact('galeria', 'users'));
        	$this->set('_serialize', ['galeria']);
        } 
    }

    /**
     * Edit method
     *
     * @param string|null $id Galeria id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $galeria = $this->Galerias->get($id, [
            'contain' => ['Fotos' => function ($q) {
            		return $q->order(['Fotos.items_order' => 'ASC']);
            }]
        ]);
        if ($this->request->is(['post'])) {
            $galeria = $this->Galerias->patchEntity($galeria, $this->request->data);
            if ($this->Galerias->save($galeria)) {
            	if ($galeria->folder != null){
            		$Fotos = $this->loadModel('Fotos');
            		$queryFotos = $Fotos->find('all',['order' => ['items_order' => 'DESC']])->first();
            		$newFotoOrderNumber = $queryFotos->items_order;
            		$Fotos->removeByGallery($galeria->id);
            		$Fotos->addByFolder($galeria,$newFotoOrderNumber);
            	}
                $this->set('success',true);
            } else {
                // send error
                $this->set('success',false);
            }
        } else {
        	$users = $this->Galerias->Users->find('list', ['limit' => 200]);
        	$this->set(compact('galeria', 'users'));
        	$this->set('_serialize', ['galeria']);
        } 
    }

    public function editOrder($id1, $id2, $newOrder1, $newOrder2){
    	if (($id1 !== null) && ($id2 !== null) && ($newOrder1 !== null) && ($newOrder2 !== null)){
    		$galeria1 = $this->Galerias->get($id1);
    		$galeria2 = $this->Galerias->get($id2);
    		if ($this->request->is(['post'])){
		   		$galeria1->items_order = $newOrder2;
		   		$galeria2->items_order = $newOrder1;
		   		if ($this->Galerias->save($galeria1) && $this->Galerias->save($galeria2)){
		   			$this->set('success',true);
	    		} else {
	    			$this->set('success',false);
	    		}
    			
    		}
    	}
    }
    /**
     * Delete method
     *
     * @param string|null $id Galeria id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $galeria = $this->Galerias->get($id);
        if ($this->request->is(['post'])){
        	if ($this->Galerias->delete($galeria)){
        		$this->set('success',true);
        		Debugger::log("Galería borrada");
        	} else {
        		$this->set('success',false);
        		Debugger::log("Galería no borrada.");
        	}
        } else {
	        if ($this->Galerias->delete($galeria)) {
	            Debugger::log("Deleted Correctly");
	        } else {
	        	Debugger::log("Error on delete");
	        }
        }
    }
    
    
}
