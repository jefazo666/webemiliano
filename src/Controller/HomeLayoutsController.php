<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * HomeLayouts Controller
 *
 * @property \App\Model\Table\HomeLayoutsTable $HomeLayouts
 */
class HomeLayoutsController extends AppController
{

	
	
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $homeLayouts = $this->paginate($this->HomeLayouts);

        $this->set(compact('homeLayouts'));
        $this->set('_serialize', ['homeLayouts']);
    }
    
    public function getToEdit($id = null){
    	$homeLayout = $this->HomeLayouts->get($id);
    	$code = $homeLayout->code;
    	$imagePlaceholder = "<div class=\"marginer\"><a><div class=\"imageHolder imageAdd\"><img foto_order=\"\\1\"></div></a></div>";
    	$code = preg_replace("/<image([0-9]+)\/>/", $imagePlaceholder, $code);
    	$homeLayout->code = $code;
    	
    	$this->set('homeLayout', $homeLayout);
    	$this->set('_serialize', ['homeLayout']);
    }

    /**
     * View method
     *
     * @param string|null $id Home Layout id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $homeLayout = $this->HomeLayouts->get($id, [
            'contain' => ['Pieces']
        ]);

        $this->set('homeLayout', $homeLayout);
        $this->set('_serialize', ['homeLayout']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $homeLayout = $this->HomeLayouts->newEntity();
        if ($this->request->is('post')) {
            $homeLayout = $this->HomeLayouts->patchEntity($homeLayout, $this->request->data);
            if ($this->HomeLayouts->save($homeLayout)) {
                $this->Flash->success(__('The home layout has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The home layout could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('homeLayout'));
        $this->set('_serialize', ['homeLayout']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Home Layout id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $homeLayout = $this->HomeLayouts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $homeLayout = $this->HomeLayouts->patchEntity($homeLayout, $this->request->data);
            if ($this->HomeLayouts->save($homeLayout)) {
                $this->Flash->success(__('The home layout has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The home layout could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('homeLayout'));
        $this->set('_serialize', ['homeLayout']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Home Layout id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $homeLayout = $this->HomeLayouts->get($id);
        if ($this->HomeLayouts->delete($homeLayout)) {
            $this->Flash->success(__('The home layout has been deleted.'));
        } else {
            $this->Flash->error(__('The home layout could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
