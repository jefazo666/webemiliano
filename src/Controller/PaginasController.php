<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Event\Event;
use Cake\Error\Debugger;

/**
 * Paginas Controller
 *
 * @property \App\Model\Table\PaginasTable $Paginas
 */
class PaginasController extends AppController
{
	public function beforeFilter(Event $event) {
		parent::beforeFilter($event);
		$this->Auth->allow("view");
	}
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
    	
    	$this->viewBuilder()->layout(false);
        $this->paginate = [
            'contain' => ['Users'], 'order' => ['items_order']
        ];
        $paginas = $this->paginate($this->Paginas);

        $this->set(compact('paginas'));
        $this->set('_serialize', ['paginas']);
    }
    
    /**
     * View method
     *
     * @param string|null $id Pagina id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
    	try {
	        $pagina = $this->Paginas->get($id, [
	            'contain' => ['Users']
	        ]);
    	} catch (RecordNotFoundException $e) {
    		return $this->redirect("/");
    	}

        $this->set('pagina', $pagina);
        $this->set('_serialize', ['pagina']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pagina = $this->Paginas->newEntity();
        if ($this->request->is('post')) {
            $pagina = $this->Paginas->patchEntity($pagina, $this->request->data);
            $pagina->user_id = $this->Auth->user('id');
            $queryAllPaginas = $this->Paginas->find('all');
            $pagina->items_order = $queryAllPaginas->count() + 1;
            if ($this->Paginas->save($pagina)) {
            	$this->set('success',true);
            	return $this->redirect(['action' => 'edit',$pagina->id]);
            } else {
            	$this->set('success',false);
            }
        } else {
	        $users = $this->Paginas->Users->find('list', ['limit' => 200]);
	        $this->set(compact('pagina', 'users'));
	        $this->set('_serialize', ['pagina']);
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Pagina id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pagina = $this->Paginas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['post'])) {
            $pagina = $this->Paginas->patchEntity($pagina, $this->request->data);
            $pagina->user_id = $this->Auth->user('id');
            if ($this->Paginas->save($pagina)) {
                $this->set('success',true);
            } else {
                $this->set('success',false);
            }
        } else {
        	$this->set(compact('pagina'));
        	$this->set('_serialize', ['pagina']);
        }
    }
    
    public function editOrder($id1, $id2, $newOrder1, $newOrder2){
    	if (($id1 !== null) && ($id2 !== null) && ($newOrder1 !== null) && ($newOrder2 !== null)){
    		$pagina1 = $this->Paginas->get($id1);
    		$pagina2 = $this->Paginas->get($id2);
    		if ($this->request->is(['post'])){
    			$pagina1->items_order = $newOrder2;
    			$pagina2->items_order = $newOrder1;
    			if ($this->Paginas->save($pagina1) && $this->Paginas->save($pagina2)){
    				$this->set('success',true);
    			} else {
    				$this->set('success',false);
    			}
    		}
    	}
    }

    /**
     * Delete method
     *
     * @param string|null $id Pagina id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null){
    	$pagina = $this->Paginas->get($id);
    	if ($this->request->is(['post'])){
    		if ($this->Paginas->delete($pagina)){
    			$this->set('success',true);
    		} else {
    			$this->set('success',false);
    		}
    	} 
    }
}
