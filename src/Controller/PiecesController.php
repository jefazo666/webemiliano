<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Error\Debugger;

/**
 * Pieces Controller
 *
 * @property \App\Model\Table\PiecesTable $Pieces
 */
class PiecesController extends AppController
{
	public function beforeFilter(Event $event){
		parent::beforeFilter($event);
		$this->Auth->allow('index');
	}

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['HomeLayouts','Fotos' => function ($q) {
            		return $q->order(['Fotos.piece_order' => 'ASC']);
            	}],
        	'maxLimit' => 5
        ];
        $pieces = $this->paginate($this->Pieces->find('all',['order' => ['items_order' => 'ASC']]));

        $basePath = "../".FotosController::BASE_DIR_FOR_PICTURES;
        $this->set(compact('pieces','basePath'));
        $this->set('_serialize', ['pieces']);
    }

    public function homeEdit(){
    	$pieces = $this->Pieces->find('all',['contain' => ['HomeLayouts','Fotos' => function ($q) {
            		return $q->order(['Fotos.piece_order' => 'ASC']);
            	}],'order' => ['items_order' => 'ASC']]);
    	
    	$basePath = "../".FotosController::BASE_DIR_FOR_PICTURES;
    	$this->set(compact('pieces','basePath'));
    	$this->set('_serialize', ['pieces']);
    }
    /**
     * View method
     *
     * @param string|null $id Piece id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $piece = $this->Pieces->get($id, [
            'contain' => ['HomeLayouts', 'Fotos' => function ($q) {
            		return $q->order(['Fotos.piece_order' => 'ASC']);
            	}]
        ]);

        $this->set('piece', $piece);
        $this->set('_serialize', ['piece']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $piece = $this->Pieces->newEntity();
        if ($this->request->is('post')) {
            $piece = $this->Pieces->patchEntity($piece, $this->request->data);
            if ($this->Pieces->save($piece)) {
                $this->Flash->success(__('The piece has been saved.'));
            	$redirectUrl = "/pieces/view/" . $piece->id . ".json";
                return $this->redirect($redirectUrl);
            } else {
                $this->Flash->error(__('The piece could not be saved. Please, try again.'));
            }
        }
        $homeLayouts = $this->Pieces->HomeLayouts->find('list', ['limit' => 200]);
        $fotos = $this->Pieces->Fotos->find('list', ['limit' => 200]);
        $this->set(compact('piece', 'homeLayouts', 'fotos'));
        $this->set('_serialize', ['piece']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Piece id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $piece = $this->Pieces->get($id, [
            'contain' => ['HomeLayouts','Fotos']
        ]);
        Debugger::log($piece);
        if ($this->request->is(['patch', 'post', 'put'])) {
        	Debugger::log("Prepatch");
        	Debugger::log($piece);
        	Debugger::log($piece->fotos);
            $piece = $this->Pieces->patchEntity($piece, $this->request->data);
            Debugger::log("postpatch");
            Debugger::log($piece);
            Debugger::log($piece->fotos);
            Debugger::log($this->request->data);
            if ($this->Pieces->save($piece)) {
                $this->Flash->success(__('The piece has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The piece could not be saved. Please, try again.'));
            }
        }
        $homeLayouts = $this->Pieces->HomeLayouts->find('list', ['limit' => 200]);
        $fotos = $this->Pieces->Fotos->find('list', ['limit' => 200]);
        $this->set(compact('piece', 'homeLayouts', 'fotos'));
        $this->set('_serialize', ['piece']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Piece id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $piece = $this->Pieces->get($id);
        if ($this->Pieces->delete($piece)) {
            $this->Flash->success(__('The piece has been deleted.'));
        } else {
            $this->Flash->error(__('The piece could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
