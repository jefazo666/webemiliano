<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
	public function beforeFilter(Event $event) {
		parent::beforeFilter($event);
		$this->Auth->allow("add");
	}
	
	public function login(){
		if ($this->request->is('post')) {
			$user = $this->Auth->identify();
			if ($user) {
				$this->Auth->setUser($user);
				$redirectUrl = $this->Auth->redirectUrl();
				if ($redirectUrl == "/")
				{
					$redirectUrl = "/pages/admin";
				}
				$this->set('redirectUrl',$redirectUrl);
				$status = 'OK';
			}
			else {
				$status = 'ERROR';
			}
			$this->set('status',$status);
			$this->render('/Element/ajaxlogin');
		}
	}
	
	public function logout(){
		$this->redirect($this->Auth->logout());
	}
     /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
	}
	
}
