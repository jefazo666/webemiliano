<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Foto Entity.
 *
 * @property int $id
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property int $galeria_id
 * @property \App\Model\Entity\Galeria $galeria
 * @property string $filename
 * @property string $path
 * @property string $title
 * @property string $description
 * @property int $piece_id
 * @property \App\Model\Entity\Piece $piece
 * @property int $piece_order
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $items_order
 */
class Foto extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
