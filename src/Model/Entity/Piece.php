<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Piece Entity.
 *
 * @property int $id
 * @property int $home_layout_id
 * @property \App\Model\Entity\HomeLayout $home_layout
 * @property int $items_order
 * @property \App\Model\Entity\HomePiece[] $home_pieces
 * @property \App\Model\Entity\Foto[] $fotos
 */
class Piece extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
