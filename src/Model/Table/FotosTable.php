<?php
namespace App\Model\Table;

use App\Model\Entity\Foto;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Fotos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Galerias
 * @property \Cake\ORM\Association\BelongsTo $Pieces
 */
class FotosTable extends Table
{
	const BASE_DIR_FOR_PICTURES = "./galleryPictures/";

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('fotos');
        $this->displayField('filename');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Galerias', [
            'foreignKey' => 'galeria_id'
        ]);
        $this->belongsTo('Pieces', [
            'foreignKey' => 'piece_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('filename', 'create')
            ->notEmpty('filename');

        $validator
            ->allowEmpty('path');

        $validator
            ->allowEmpty('title');

        $validator
            ->allowEmpty('description');

        $validator
            ->integer('piece_order')
            ->allowEmpty('piece_order');

        $validator
            ->integer('items_order')
            ->requirePresence('items_order', 'create')
            ->notEmpty('items_order');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['galeria_id'], 'Galerias'));
        $rules->add($rules->existsIn(['piece_id'], 'Pieces'));
        return $rules;
    }
    
    public function addFile($file,$galeria,$orderNumber){
    	if ($file != null && $galeria->id != null) {
    		$fotosTable = TableRegistry::get('Fotos');
    		$foto = $fotosTable->newEntity();
    		$foto->user_id = $galeria->user_id;
    		$foto->filename = $file;
    		$foto->path = $galeria->folder . "/";
    		$foto->galeria_id = $galeria->id;
    		$foto->items_order = $orderNumber;
    		$fotoPath = "./". self::BASE_DIR_FOR_PICTURES .$foto->path.$foto->filename;
    		if ($fotosTable->save($foto)) {
    			// success
    		} else {
    			// 	send error
    		}
    	}
    }  

    public function removeByGallery($galleryId = null){
    	if ($galleryId != null){
    		$fotos = TableRegistry::get('Fotos');
    		$fotos->deleteAll([
    				'galeria_id' => $galleryId
    		]);
    	}
    }
    
	public function addByFolder($galeria,$orderNumber) {
    	if ($galeria->folder != null && $galeria->id != null){
    		if ($handle = opendir( self::BASE_DIR_FOR_PICTURES . $galeria->folder)) {
    			while (false !== ($entry = readdir($handle))) {
    				if ($entry != "." && $entry != "..") {
    					if (is_dir(self::BASE_DIR_FOR_PICTURES . $galeria->folder."/".$entry) == false) {
    						$orderNumber++;
    						self::addFile($entry,$galeria,$orderNumber);
    					}
    				}
    			}
    			closedir($handle);
    		}	
    	}
    }
}
