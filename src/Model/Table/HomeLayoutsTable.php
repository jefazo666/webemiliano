<?php
namespace App\Model\Table;

use App\Model\Entity\HomeLayout;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * HomeLayouts Model
 *
 * @property \Cake\ORM\Association\HasMany $Pieces
 */
class HomeLayoutsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('home_layouts');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->hasMany('Pieces', [
            'foreignKey' => 'home_layout_id'
        ]);
    }
    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('preview', 'create')
            ->notEmpty('preview');

        $validator
            ->integer('count')
            ->allowEmpty('count');

        $validator
            ->requirePresence('code', 'create')
            ->notEmpty('code');

        return $validator;
    }
}
