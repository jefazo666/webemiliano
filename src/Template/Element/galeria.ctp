<div class="grid_12 alpha omega">
	<div class="grid_3 alpha" id="content-left">
		<div class="top pagination">
			<a rel="history" href="#11" title="Anterior" class="direction-buttons prev inactive-link"><?= $this->Html->image('arrow_left.png',["class" => "navigation", "id" => "prev"]); ?></a>
			<a rel="history" href="#1" title="0" class="current number">1</span>
			<a rel="history" href="#11" title="1" class="number">2</a>
			<a rel="history" href="#21" title="2" class="number">3</a>
			<a rel="history" href="#21" title="3" class="number">4</a>
			<a rel="history" href="#11" title="Siguiente" class="direction-buttons next"><?= $this->Html->image('arrow_right.png',["class" => "navigation", "id" => "prev"]); ?></a>
		</div>
		<ul class="thumbnails visible">
			<li target="/webEmi/img/foto.jpg"><a><?= $this->Html->image('th.jpg');?></a></li>
			<li target="/webEmi/img/foto1.jpg"><a><?= $this->Html->image('th1.jpg');?></a></li>
			<li target="/webEmi/img/foto2.jpg"><a><?= $this->Html->image('th2.jpg');?></a></li>
			<li target="/webEmi/img/foto3.jpg"><a><?= $this->Html->image('th3.jpg');?></a></li>
		</ul>
		<ul class="thumbnails">
			<li target="/webEmi/img/foto1.jpg"><a><?= $this->Html->image('th1.jpg');?></a></li>
			<li target="/webEmi/img/foto1.jpg"><a><?= $this->Html->image('th1.jpg');?></a></li>
			<li target="/webEmi/img/foto1.jpg"><a><?= $this->Html->image('th1.jpg');?></a></li>
			<li target="/webEmi/img/foto1.jpg"><a><?= $this->Html->image('th1.jpg');?></a></li>
		</ul>
		<ul class="thumbnails">
			<li target="/webEmi/img/foto2.jpg"><a><?= $this->Html->image('th2.jpg');?></a></li>
			<li target="/webEmi/img/foto2.jpg"><a><?= $this->Html->image('th2.jpg');?></a></li>
			<li target="/webEmi/img/foto2.jpg"><a><?= $this->Html->image('th2.jpg');?></a></li>
			<li target="/webEmi/img/foto2.jpg"><a><?= $this->Html->image('th2.jpg');?></a></li>
		</ul>
		<ul class="thumbnails">
			<li target="/webEmi/img/foto3.jpg"><a><?= $this->Html->image('th3.jpg');?></a></li>
			<li target="/webEmi/img/foto3.jpg"><a><?= $this->Html->image('th3.jpg');?></a></li>
			<li target="/webEmi/img/foto3.jpg"><a><?= $this->Html->image('th3.jpg');?></a></li>
		</ul>
	</div>
	<div class="grid_9 omega" id="content-right">
		<div class="slideshow front">
			<div class="image_centerer">
				<?= $this->Html->image('foto.jpg'); ?>
			</div>
		</div>
		<div class="slideshow back">
			<div class="image_centerer">
				<?= $this->Html->image('foto3.jpg'); ?>
			</div>
		</div>
	</div>
</div>