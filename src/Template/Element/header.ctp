<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $siteTitleLocal ?>
    </title>
    <?= $this->Html->script('http://code.jquery.com/jquery.min.js'); ?>
    <?= $this->Html->script('galery.js'); ?>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('grid.css') ?>
    <?= $this->Html->css('emiliano.css') ?>
</head>