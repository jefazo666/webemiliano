<?= $this->Form->create($foto,['id'=>'createFotoForm']) ?>
<? ?>
<table class="editor_table" foto_id="<?= $foto->id ?>">
	<thead>
		<tr>
			<th colspan="2">Datos de la fotografía - Editar</th>
		</tr>
	</thead>
	<tr>
		<td class="img_preview" colspan="2"><img src="/galleryPictures/<?= $foto->path . $foto->filename; ?>">
		</td>
	</tr>
	<tr>
		<td>Nombre</td>
		<td><?= $this->Form->input('title',['label' => false, 'id' => 'title']); ?></td>
	</tr>
	<tr>
		<td>Descripcion</td>
		<td><?= $this->Form->input('description',['label' => false, 'id' => 'description']); ?></td>
	</tr>
	<tr>
		<td>Galería</td>
		<td><?= $this->Form->input('galeria_id',['label' => false, 'options' => $galerias, 'id' => 'galeria_id','empty' => 'Sin galería']); ?></td>
	</tr>
	<tr>
		<td colspan="2">
			<?= $this->Form->button(__('Guardar'), ['type' => 'button', 'class' => 'submit', 'onclick' => '', 'id' => 'saveFotoButton', 'imagePath' => $foto->path]) ?>
		</td>
	</tr>
<?= $this->Form->end() ?>
</table>