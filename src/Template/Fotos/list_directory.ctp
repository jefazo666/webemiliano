<table class="files_list" path="<?= $path ?>">
	<thead>
		<tr>
			<th>Foto</th>
			<th>Ruta</th>
			<th>Titulo</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($files as $file): ?>
	<? if ($file['isDir']) {?>
	<tr class="directory" dirName="<?= $file['file']; ?>/">
	<? 			
	} else {
		if ($file['included']) { ?>
	<tr class="file file_included" foto_id="<?= $file['id'] ?>">
		<? } else { ?>
	<tr class="file file_not_included" fileName="<?= $file['file'] ?>">
		<? 
		}
	} ?>
		<td><img src="/img/admin/picture-placeholder.png"></td>
		<td class="file_name"><?= $file['file'] ?></td>
		<td class="foto_name"><?= $file['title'] ?></td>
		<td class="foto_action">
		<? if (!$file['isDir']) {
			if ($file['included']) { 
		?>
		<button class="action edit" foto_id="<?= $file['id'] ?>"><img src="/img/admin/edit.png"></button>
		<button class="action delete" foto_id="<?= $file['id'] ?>"><img src="/img/admin/delete.png"></button>
		<? } else { ?>
		<button class="action add"><img src="/img/admin/add.png"></button>
		<? }
		}?>
		</td>
	</td>
	<? endforeach;?>	
	</tbody>
</table>