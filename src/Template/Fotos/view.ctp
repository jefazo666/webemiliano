<?php
$this->assign('title',"Emiliano Cossini");
$this->Galeria->setBasePath($basePath);
?>
    <?= $this->Html->css('galeria.css')?>
    <?= $this->Html->script('jquery.min.js') ?>
    <?= $this->Html->script('more.js') ?>
    <?= $this->Html->script('gallery.js') ?>
    	<div class="container_15">
    		<div class="grid_15 no_overflow">
    			<div class="gallery">
    				<?php 
    				echo $this->Galeria->image($foto,null, null,true);
            		?>
    			</div>
    		</div>
    	</div>
