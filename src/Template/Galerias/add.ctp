<?
if (!isset($success)) { 
?>
<?= $this->Form->create($galeria,["id" => "galleryForm"]); ?>
<table class="editor_table">
	<thead>
		<tr>
			<th colspan="2">Datos de la galería</th>
		</tr>
	</thead>
	<tbody>
	
	<tr>
		<td>Nombre</td>
		<td><?= $this->Form->input('title',['label' => false, 'id' => 'title']); ?></td>
	</tr>
	<tr>
		<td>Descripción</td>
		<td><?= $this->Form->input('description',['label' => false, 'id' => 'description']); ?></td>
	</tr>
	<tr>
		<td>Folder</td>
		<td><?= $this->Form->input('folder',['label' => false, 'id' => 'folder']); ?></td>
	</tr>
	</tbody>
	<tfoot>
	<tr>
		<td colspan="2">
			<?= $this->Form->button(__('Guardar'),['type' => 'button', 'class' => 'submit', 'onclick' => '', 'id' => 'saveGaleriaButton']); ?>
		</td>
	</tr>
	</tfoot>
</table>
<?= $this->Form->end(); ?>
		<table class="editor_table">
			<thead>
				<tr>
					<th class="preview">Foto</th>
					<th class="id">ID</th>
					<th class="path">Ruta</th>
					<th class="title">Título</th>
					<th class="action">Action</th>
				</tr>
			</thead>
			<tbody>			
		</tbody></table>
<? 
} else {
	echo $success;
}
?>