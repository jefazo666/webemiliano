<table class="galerias_list">
	<thead>
		<tr>
			<th>ID</th>
			<th>Nombre</th>
			<th>Fotos</th>
			<th>Descripción</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<? foreach ($galerias as $galeria) { ?>
		<tr galeria_order="<?= $galeria->items_order ?>" galeria_id="<?= $galeria->id ?>">
			<td><?= $galeria->id ?></td>
			<td><?= $galeria->title?></td>
			<td><?= count($galeria->fotos) ?></td>
			<td><?= $galeria->description ?></td>
			<td>
				<button class="action edit" galeria_id="<?= $galeria->id ?>"><img src="/img/admin/edit.png"></button>
				<button class="action delete" galeria_id="<?= $galeria->id ?>"><img src="/img/admin/delete.png"></button>
				<button class="action up" galeria_id="<?= $galeria->id ?>"><img src="/img/admin/up.png"></button>
				<button class="action down" galeria_id="<?= $galeria->id ?>"><img src="/img/admin/down.png"></button>
			</td>
		</tr>
		<? } ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="5">
				<button id="newGaleriaButton">Nueva</button>
			</td>
		</tr>
	</tfoot>
</table>