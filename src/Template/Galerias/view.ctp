<?php

$this->Galeria->setBasePath($basePath);
$this->assign('title',$galeria->title);
?>
    <?= $this->Html->css('galeria.css')?>
    <?= $this->Html->script('jquery.min.js') ?>
    <?= $this->Html->script('more.js') ?>
    <?= $this->Html->script('gallery.js') ?>
    	<div class="container_15">
    		<div class="grid_15 no_overflow">
    			<div class="gallery" gallery_id="<?= $galeria->id ?>" gallery_length="<?= count($galeria->fotos) ?>">
    				<?php
    				$i = 0; 
    				$current = $offset;
    				foreach ($galeria->fotos as $foto):
    					
    					if (($i >= $offset - $fotosLimit) && ($i <= $offset + $fotosLimit)) {
	    					if ($i == $current) {
	            				$this->Galeria->image($foto,$i, null,true);
	    					} else {
	    						if ($i < $current) {
	    							$this->Galeria->image($foto, $i, "p");
	    						} else {
	    							$this->Galeria->image($foto, $i, "n");
	    						}
	    					}
    					}
    					$i++;
            		endforeach;
            		?>
    			</div>
    			<div class="gallery_controls">
    				<div class="gallery_control left">
    					<div class="icon_padding">
    						<div class="icon"></div>
    					</div>
    				</div>
    					
    				<div class="gallery_control right">
    					<div class="icon_padding">
    						<div class="icon"></div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
