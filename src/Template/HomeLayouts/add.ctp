<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Home Layouts'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Pieces'), ['controller' => 'Pieces', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Piece'), ['controller' => 'Pieces', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="homeLayouts form large-9 medium-8 columns content">
    <?= $this->Form->create($homeLayout) ?>
    <fieldset>
        <legend><?= __('Add Home Layout') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('preview');
            echo $this->Form->input('count');
            echo $this->Form->input('code');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
