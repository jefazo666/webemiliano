<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Home Layout'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pieces'), ['controller' => 'Pieces', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Piece'), ['controller' => 'Pieces', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="homeLayouts index large-9 medium-8 columns content">
    <h3><?= __('Home Layouts') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th><?= $this->Paginator->sort('preview') ?></th>
                <th><?= $this->Paginator->sort('count') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($homeLayouts as $homeLayout): ?>
            <tr>
                <td><?= $this->Number->format($homeLayout->id) ?></td>
                <td><?= h($homeLayout->name) ?></td>
                <td><?= h($homeLayout->preview) ?></td>
                <td><?= $this->Number->format($homeLayout->count) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $homeLayout->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $homeLayout->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $homeLayout->id], ['confirm' => __('Are you sure you want to delete # {0}?', $homeLayout->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
