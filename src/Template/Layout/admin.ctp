<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= "Administración" ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    
	<?= $this->Html->script('jquery.min.js') ?>
	<?= $this->Html->script('admin.js') ?>
	
    
    <?= $this->Html->css('admin.css') ?>
    <?= $this->Html->css('grid.css') ?>
    <?= $this->Html->css('fancyNav.css')?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body class="admin">
    <div class="container_15 main">
    	<div class="top grid_15">
			<nav id="cssmenu">
				<ul>
					<li><a href="/" id="home">home</a></li>
					<li><a href="/pieces/homeEdit">Edit home</a>
					<li><a id="paginas">Páginas</a></li>
					<li><a id="galerias">Galerías</a></li>
					<li><a id="fotos">Fotos</a></li>
					<li class="login"><a href="/users/logout"><?
						 $user = $this->request->session()->read('Auth.User');
						if(!empty($user)) {
		    				echo $user['email'];
							}
						else {
							echo "Desconocido";
						} 
					?></a></li>
					<li id="info"><a></a></li>
				</ul>
			</nav>
			
		</div>
		<div class="content grid_15">
			<?= $this->fetch('content') ?>
		</div> 
    </div>
    <footer>
    </footer>
</body>
</html>