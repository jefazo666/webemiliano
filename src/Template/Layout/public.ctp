<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>emilianocossini.com</title>
    <?= $this->Html->meta('icon') ?>    
	<?= $this->Html->script('jquery.min.js') ?>
	<?= $this->Html->css('nav.css') ?>
	<?= $this->Html->css('general.css') ?>
	<?= $this->Html->css('grid.css') ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    
</head>
<body>
    <div id="top">
    	<?= $this->cell('Topbar') ?>
    </div>
    <div id="content">		
		<?= $this->fetch('content') ?>
    </div>
    <footer>
    	
    </footer>
</body>
</html>