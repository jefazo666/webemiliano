<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Exception\NotFoundException;



if (!Configure::read('debug')):
    throw new NotFoundException('Please replace src/Template/Pages/home.ctp with your own version.');
endif;

$cakeDescription = 'CakePHP: the rapist development PHP framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('grid.css') ?>
    <?= $this->Html->css('nav.css') ?>
    <?= $this->Html->css('general.css') ?>
    <?= $this->Html->css('galeria.css')?>
    <?= $this->Html->script('jquery.min.js'); ?>
    <?= $this->Html->script('gallery.js'); ?>
    
</head>
<body class="home">
	<div id="top">
    	<div class="container_15">
    		<div class="grid_15">
    		<ul>
    			<li>Glearia 1</li>
    			<li>Gleari 2</li>
    		</ul>
    		</div>
    	</div>
    </div>
    <div id="content">
    	<div class="container_15">
    		<div class="grid_15 no_overflow">
    			<div class="gallery">
    				<div class="gallery_item p">
	    				<div class="image_centerer">
	    					<div class="image_placeholder">
	    						<img src="/webEmi/img/midl1.jpg">
	    					</div>
	    				</div>
    				</div>
    				<div class="gallery_item" id="c">
	    				<div class="image_centerer">
	    					<div class="image_placeholder">
	    						<img src="/webEmi/img/small1.jpg">
	    					</div>
	    				</div>
    				</div>
    				<div class="gallery_item n">
	    				<div class="image_centerer">
	    					<div class="image_placeholder">
	    						<img src="/webEmi/img/small2.jpg">
	    					</div>
	    				</div>
    				</div>
    				<div class="gallery_control left">
    					<div class="icon_padding">
    						<div class="icon"></div>
    					</div>
    				</div>
    					
    				<div class="gallery_control right">
    					<div class="icon_padding">
    						<div class="icon"></div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    <div id="footer">
    	<div class="container_15">
    		<div class="grid_15">
    		Created by me.
    		</div>
    	</div>
    </div>
</body>
</html>