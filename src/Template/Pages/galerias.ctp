<div class="items_list grid_5 alpha">
		<table class="items_list_table">
			<thead>
				<tr>
					<th>ID</th>
					<th>Nombre</th>
					<th>Fotos</th>
					<th>Activo</th>
					<th colspan="3">Action</th>
				</tr>
			</thead>
			<tr>
				<td>1</td>
				<td>China</td>
				<td>23</td>
				<td>Si</td>
				<td><button class="action"><img src="/webEmi/img/admin/unpublish.png"></button></td>
				<td><button class="action"><img src="/webEmi/img/admin/edit.png"></button></td>
				<td><button class="action"><img src="/webEmi/img/admin/delete.png"></button></td>
			</tr>
			<tr>
				<td>2</td>
				<td>Torres</td>
				<td>3</td>
				<td>Si</td>
				<td><button class="action"><img src="/webEmi/img/admin/unpublish.png"></button></td>
				<td><button class="action"><img src="/webEmi/img/admin/edit.png"></button></td>
				<td><button class="action"><img src="/webEmi/img/admin/delete.png"></button></td>
			</tr>
			<tr>
				<td>3</td>
				<td>Marruecos</td>
				<td>36</td>
				<td>Si</td>
				<td><button class="action"><img src="/webEmi/img/admin/unpublish.png"></button></td>
				<td><button class="action"><img src="/webEmi/img/admin/edit.png"></button></td>
				<td><button class="action"><img src="/webEmi/img/admin/delete.png"></button></td>
			</tr>
			
		</table>
	</div>
	<div class="editor grid_5 editor_galery">
		<table class="editor_table">
			<thead>
				<tr>
					<th colspan="2">Datos de la galería</th>
				</tr>
			</thead>
			<tr>
				<td>Nombre</td>
				<td><input type="text"></td>
			</tr>
			<tr>
				<td>Descripcion</td>
				<td><textarea rows="3" cols="35"></textarea></td>
			</tr>
			<tr>
				<td colspan="2">
					<button>Guardar</button>
				</td>
			</tr>
				
		</table>
		<table class="editor_table">
			<thead>
				<tr>
					<th class="preview">Foto</th>
					<th class="id">ID</th>
					<th class="path">Ruta</th>
					<th class="action">Action</th>
				</tr>
			</thead>
			<tr>
				<td><img src="/webEmi/img/admin/picture-placeholder.png"></td>
				<td>1</td>
				<td>fotografia 1</td>
				<td><button class="action"><img src="/webEmi/img/admin/remove.png"></button></td>
			</tr>
			<tr>
				<td><img src="/webEmi/img/admin/picture-placeholder.png"></td>
				<td>2</td>
				<td>marruecos/loquesea.png</td>
				<td><button class="action"><img src="/webEmi/img/admin/remove.png"></button></td>
			</tr>
			<tr>
				<td><img src="/webEmi/img/admin/picture-placeholder.png"></td>
				<td>3</td>
				<td>fotografia/fsadfas/dsfa.jpg</td>
				<td><button class="action"><img src="/webEmi/img/admin/remove.png"></button></td>
			</tr>
			
		</table>
	</div>
	<div class="files grid_5 omega">
		<table class="files_table">
			<thead>
				<tr>
					<th class="preview">Foto</th>
					<th class="id">ID</th>
					<th class="path">Ruta</th>
					<th class="action">Action</th>
				</tr>
			</thead>
			<tr>
				<td><img src="/webEmi/img/admin/picture-placeholder.png"></td>
				<td>1</td>
				<td>fotografia 1</td>
				<td><button class="action"><img src="/webEmi/img/admin/add.png"></button></td>
			</tr>
			<tr>
				<td><img src="/webEmi/img/admin/picture-placeholder.png"></td>
				<td>2</td>
				<td>marruecos/loquesea.png</td>
				<td><button class="action"><img src="/webEmi/img/admin/add.png"></button></td>
			</tr>
			<tr>
				<td><img src="/webEmi/img/admin/picture-placeholder.png"></td>
				<td>3</td>
				<td>fotografia/fsadfas/dsfa.jpg</td>
				<td><button class="action"><img src="/webEmi/img/admin/add.png"></button></td>
			</tr>		
		</table>
	</div>