<? 
	$this->assign('title', 'Panel de administrador'); 
?>

	<div class="items_list grid_5 alpha">
		<table class="items_list_table">
			<thead>
				<tr>
					<th>ID</th>
					<th>Nombre</th>
					<th>Activo</th>
					<th colspan="3">Action</th>
				</tr>
			</thead>
			<tr>
				<td>1</td>
				<td>About</td>
				<td>Si</td>
				<td><button><img src="/webEmi/img/admin/unpublish.png"></button></td>
				<td><button><img src="/webEmi/img/admin/edit.png"></button></td>
				<td><button><img src="/webEmi/img/admin/delete.png"></button></td>
			</tr>
			<tr>
				<td>2</td>
				<td>Contact</td>
				<td>Si</td>
				<td><button><img src="/webEmi/img/admin/unpublish.png"></button></td>
				<td><button><img src="/webEmi/img/admin/edit.png"></button></td>
				<td><button><img src="/webEmi/img/admin/delete.png"></button></td>
			</tr>
			<tr>
				<td>3</td>
				<td>Other</td>
				<td>Si</td>
				<td><button><img src="/webEmi/img/admin/unpublish.png"></button></td>
				<td><button><img src="/webEmi/img/admin/edit.png"></button></td>
				<td><button><img src="/webEmi/img/admin/delete.png"></button></td>
			</tr>
			
		</table>
	</div>
	<div class="editor grid_10 omega">
		<div class="editor_pages">
			<input type="text" class="editor_pages">
		</div>
	</div>
