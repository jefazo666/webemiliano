<?
if (!isset($success)) { 
?>
<?= $this->Form->create($pagina,["id" => "paginaForm"]); ?>
<table class="editor_table">
	<thead>
		<tr>
			<th colspan="2">Editor de páginas</th>
		</tr>
	</thead>
	<tbody>
	
	<tr>
		<td>Título</td>
		<td><?= $this->Form->input('title',['label' => false, 'id' => 'title']); ?></td>
	</tr>
	</tbody>
	<thead>
	<tr>
		<td colspan="2">Contenido</td>
	</tr>
	</thead>
	<tbody>
	<tr>
		<td colspan="2"><?= $this->Form->textarea('texto',['label' => false, 'id' => 'texto']); ?></td>
	</tr>
	</tbody>
	<tfoot>
	<tr>
		<td colspan="2">
			<?= $this->Form->button(__('Guardar'),['type' => 'button', 'class' => 'submit', 'onclick' => '', 'id' => 'savePaginaButton']); ?>
		</td>
	</tr>
	</tfoot>
</table>
<?= $this->Form->end(); ?>
<? 
} else {
	echo $success;
}
?>