<table class="paginas_list">
	<thead>
		<tr>
			<th>Id</th>
			<th>Nombre</th>
			<th>Acción</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($paginas as $pagina): ?>
		<tr pagina_order="<?= $pagina->items_order ?>" pagina_id="<?= $pagina->id ?>">
			<td><?= $this->Number->format($pagina->id) ?></td>
			<td><?= h($pagina->title) ?></td>
			<td>
				<button class="edit"
					pagina_id="<?= $this->Number->format($pagina->id) ?>">
					<img src="/img/admin/edit.png">
				</button>
				<button class="delete"
					pagina_id="<?= $this->Number->format($pagina->id) ?>">
					<img src="/img/admin/delete.png">
				</button>
				<button class="action up" 
					pagina_id="<?= $this->Number->format($pagina->id) ?>">
					<img src="/img/admin/up.png">
				</button>
				<button class="action down" 
					pagina_id="<?= $this->Number->format($pagina->id) ?>">
					<img src="/img/admin/down.png">
				</button>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="5">
				<button id="newPaginaButton">Nueva</button>
			</td>
		</tr>
	</tfoot>
</table>
