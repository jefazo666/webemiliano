    <?= $this->Html->css('home.css') ?>        
    <?php $this->assign('title',"Home Page Edit"); ?>
    <?= $this->Html->css('home_admin.css')?>
    <?= $this->Html->script('home_admin.js')?>
    <?php
    
    foreach ($pieces as $piece) {
    	$code = $piece->home_layout->code;
    	$code = preg_replace("/PIECE_ID/",$piece->id,$code);
    	$i = 1;
    	foreach ($piece->fotos as $foto){
    		$linkCode;
    		
    		$holderCode	= "<div class=\"marginer\"><a><div class=\"imageHolder imageAdd imageAdded\"><img src=\"$basePath$foto->path$foto->filename\" foto_order=\"$i\" foto_id=\"$foto->id\" piece_id=\"$piece->id\"></div></a></div>";
    		$fotoCode = $holderCode;
    		$code = preg_replace("/<image".$i."\/>/", $fotoCode , $code);
    		$i++;
    	}
    	$imagePlaceholder = "<div class=\"marginer\"><a><div class=\"imageHolder imageAdd\"><img foto_order=\"\\1\" piece_id=\"$piece->id\"></div></a></div>";
    	$code = preg_replace("/<image([0-9]+)\/>/", $imagePlaceholder, $code);
    	echo $code;
    }
    ?>
	<div class="container_15 ">
		<div class="grid_15 frame_9">
			<a>
				<div id="layoutPlaceholder" class="imageHolder">
					<img>
				</div>
			</a>
			<div id="layoutBrowser">
				<ul id="layoutList">
				</ul>
			</div>
		</div>
	</div>
