<?php
$this->HomeImage->setBasePath($basePath);
$this->assign('title',"Emiliano Cossini");

if ($this->Paginator->counter(['format' => '{{page}}']) == 1 ) {
    echo $this->Html->css('home.css');        
    echo $this->Html->script('home.js');
}
	foreach ($pieces as $piece) {
		$code = $piece->home_layout->code;
		$i = 1;
		foreach ($piece->fotos as $foto){
			$linkCode;
			if ($foto->galeria_id == null) { 
			    $linkCode = "<div class=\"marginer\"><a href=\"fotos/view/$foto->id\">";
			} else { 
			   	$linkCode = "<div class=\"marginer\"><a href=\"galerias/view/$foto->galeria_id/$foto->id\">";
			} 
			$holderCode	= "<div class=\"imageHolder\"><img src=\"$basePath$foto->path$foto->filename\" foto_id=\"$foto->id\"></div>"; 
			$fotoCode = $linkCode.$holderCode."</a></div>";
			$code = preg_replace("/<image".$i."\/>/", $fotoCode , $code);
			$i++;
		}
		echo $code;
 	}
 	
?>