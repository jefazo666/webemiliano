<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Piece'), ['action' => 'edit', $piece->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Piece'), ['action' => 'delete', $piece->id], ['confirm' => __('Are you sure you want to delete # {0}?', $piece->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pieces'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Piece'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Home Layouts'), ['controller' => 'HomeLayouts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Home Layout'), ['controller' => 'HomeLayouts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Fotos'), ['controller' => 'Fotos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Foto'), ['controller' => 'Fotos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pieces view large-9 medium-8 columns content">
    <h3><?= h($piece->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Home Layout') ?></th>
            <td><?= $piece->has('home_layout') ? $this->Html->link($piece->home_layout->name, ['controller' => 'HomeLayouts', 'action' => 'view', $piece->home_layout->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($piece->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Items Order') ?></th>
            <td><?= $this->Number->format($piece->items_order) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Fotos') ?></h4>
        <?php if (!empty($piece->fotos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Galeria Id') ?></th>
                <th><?= __('Filename') ?></th>
                <th><?= __('Path') ?></th>
                <th><?= __('Title') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Orientation') ?></th>
                <th><?= __('Piece Id') ?></th>
                <th><?= __('Piece Order') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th><?= __('Items Order') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($piece->fotos as $fotos): ?>
            <tr>
                <td><?= h($fotos->id) ?></td>
                <td><?= h($fotos->user_id) ?></td>
                <td><?= h($fotos->galeria_id) ?></td>
                <td><?= h($fotos->filename) ?></td>
                <td><?= h($fotos->path) ?></td>
                <td><?= h($fotos->title) ?></td>
                <td><?= h($fotos->description) ?></td>
                <td><?= h($fotos->orientation) ?></td>
                <td><?= h($fotos->piece_id) ?></td>
                <td><?= h($fotos->piece_order) ?></td>
                <td><?= h($fotos->created) ?></td>
                <td><?= h($fotos->modified) ?></td>
                <td><?= h($fotos->items_order) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Fotos', 'action' => 'view', $fotos->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Fotos', 'action' => 'edit', $fotos->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Fotos', 'action' => 'delete', $fotos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $fotos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
