<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Exception\NotFoundException;

$this->layout = false;

?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= "Emiliano - Administración" ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('grid.css') ?>
    <?= $this->Html->css('general.css') ?>
    <?= $this->Html->css('login.css'); ?>
    <?= $this->Html->script('jquery.min.js'); ?>
    <?= $this->Html->script('http://code.jquery.com/ui/1.11.4/jquery-ui.js')?>
    <?= $this->Html->script('login.js'); ?>
    
</head>
<body class="login">
	<div class="login_background">
		<div class="login_square">
			<div class="login_logo">
				<a id="logoImage">
					<div class="logo accelerated">
						<?= $this->Html->image('login/estrella.png'); ?>
					</div>
				</a>
			</div>
			<div class="login_form">
				<div class="login_form_centerer">
				<?= $this->Form->create() ?>
				<div class="input_row"><?= $this->Form->input('email', ['label' => false, 'placeholder' => 'Usuario']) ?></div>
				<div class="input_row"><?= $this->Form->input('password' , ['label' => false, 'placeholder' => 'contrasenha'])?></div>
				<?= $this->Form->button('Entrar', ['class' => 'login'])?>
				<div class="form_message"></div>
				<?= $this->Form->end() ?>
				</div>
			</div>
		</div>
	</div>
</body>
</html>