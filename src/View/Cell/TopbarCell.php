<?
namespace App\View\Cell;

use Cake\View\Cell;

class TopbarCell extends Cell
{

    public function display()
    {
    	$this->loadModel('Galerias');
    	$galerias = $this->Galerias->find('all', ['order' => 'items_order']);
    	
    	$this->loadModel('Paginas');
    	//$paginas = $this->Paginas->find('all');
    	$paginas = $this->Paginas->find('all',['order' =>'items_order']);
    	
    	$this->set('galerias',$galerias);
    	$this->set('paginas',$paginas);
    }
}

?>