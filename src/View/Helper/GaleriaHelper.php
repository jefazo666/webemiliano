<? 
namespace App\View\Helper;

use Cake\View\Helper;

class GaleriaHelper extends Helper {
	
	public $helpers = ['Html'];
	public $basePath = "./";
	
	public function setBasePath($newBasePath) {
		$this->basePath = $newBasePath;
	}
    
    public function image($foto,$orderNumber,$aditionalClass = null, $isCurrent = null)
    {
    	$class = "gallery_item";
    	if ($aditionalClass !== null) { $class = $class . " " . $aditionalClass; }
    	?>
    		<div <? 
    			echo " class=\"$class\" ";
    			if ($orderNumber !== null ) { echo " orderNumber=\"$orderNumber\" "; }
        		if ($isCurrent != null && $isCurrent) { echo " id=\"c\" "; }
    		?>>
    			<div class="centerer">
	        		<div class="image_centerer">
	        			<div class="image_placeholder">
	        		<?= $this->Html->image($this->basePath.$foto->path.$foto->filename) ?>
	        			</div>
	        		</div>
        		</div>
        		<div class="grid_11 push_2">
        		<div class="foto_info">
    	    		<? if ($foto->title != null) :?>
    	    		<div>
    	    			<h1><?= $foto->title ?></h1>
    	    		</div>
    	    		<? endif; ?>
    	    		<? if ($foto->description != null) :?>
    	    		<div>
    	    			<p><?= $foto->description ?></p>
    	    		</div>
    	    		<? endif; ?>
        		</div>
        		</div>
        	</div>	

        	<? 	
        }
}
?>
