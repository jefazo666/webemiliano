<? 
namespace App\View\Helper;

use Cake\View\Helper;

class HomeImageHelper extends Helper {
	
	public $helpers = ['Html'];
	public $basePath = "./";
	
	public function setBasePath($newBasePath) {
		$this->basePath = $newBasePath;
	}
	
    public function image($foto)
    {
    	if ($foto->galeriaId == null){ ?>
    		<a href="fotos/view/<?= $foto->id ?>">
    	<?php } else { ?>
    		<a href="galerias/view/<?= $foto->galeriaId."/".$foto->id ?>">
    	<?php  } ?>
				<div class="imageHolder"><?= 
					$this->Html->image($this->basePath.$foto->path.$foto->filename,
						['foto_id' => $foto->id]); 
				?></div> 
			</a>
    	<? 	
    }
}
?>