<?php
namespace App\Test\TestCase\Controller;

use App\Controller\FotosController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\FotosController Test Case
 */
class FotosControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fotos',
        'app.users',
        'app.galerias',
        'app.pieces',
        'app.home_layouts'
    ];

    /**
     * Test beforeFilter method
     *
     * @return void
     */
    public function testBeforeFilter()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test gallery method
     *
     * @return void
     */
    public function testGallery()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test listDirectory method
     *
     * @return void
     */
    public function testListDirectory()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test noGaleria method
     *
     * @return void
     */
    public function testNoGaleria()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test viewByPath method
     *
     * @return void
     */
    public function testViewByPath()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getGalleryItem method
     *
     * @return void
     */
    public function testGetGalleryItem()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test editOrder method
     *
     * @return void
     */
    public function testEditOrder()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
