<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\HomeLayoutsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\HomeLayoutsTable Test Case
 */
class HomeLayoutsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\HomeLayoutsTable
     */
    public $HomeLayouts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.home_layouts',
        'app.pieces',
        'app.home_pieces',
        'app.fotos',
        'app.users',
        'app.galerias',
        'app.pieces_fotos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('HomeLayouts') ? [] : ['className' => 'App\Model\Table\HomeLayoutsTable'];
        $this->HomeLayouts = TableRegistry::get('HomeLayouts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->HomeLayouts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
