<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\HomePiecesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\HomePiecesTable Test Case
 */
class HomePiecesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\HomePiecesTable
     */
    public $HomePieces;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.home_pieces',
        'app.pieces',
        'app.home_layouts',
        'app.fotos',
        'app.users',
        'app.galerias',
        'app.pieces_fotos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('HomePieces') ? [] : ['className' => 'App\Model\Table\HomePiecesTable'];
        $this->HomePieces = TableRegistry::get('HomePieces', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->HomePieces);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
