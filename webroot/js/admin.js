$(function() {
	var paginasSelector = "#cssmenu #paginas";
	var galeriasSelector = "#cssmenu #galerias";
	var fotosSelector = "#cssmenu #fotos";
	
	var dirItemViewSelector = "table.files_list tr.directory";
	
	var fileItemEditSelector = "table.files_list button.edit";
	var fileItemDeleteSelector = "table.files_list button.delete";
	var fileItemAddSelector = "table.files_list button.add";
	
	var galeriaItemEditSelector = "table.galerias_list button.edit";
	var galeriaItemDeleteSelector = "table.galerias_list button.delete";
	
	var galeriaUpSelector = "table.galerias_list button.up";
	var galeriaDownSelector = "table.galerias_list button.down";
	var paginaUpSelector = "table.paginas_list button.up";
	var paginaDownSelector = "table.paginas_list button.down";
	var fotoUpSelector = "table.foto_list button.up";
	var fotoDownSelector = "table.foto_list button.down";
	
	var paginaItemEditSelector = "table.paginas_list button.edit";
	var paginaItemDeleteSelector = "table.paginas_list button.delete";
	
	var paginaNewButtonSelector = "#newPaginaButton";
	var galeriaNewButtonSelector = "#newGaleriaButton";
	
	var paginaSaveButtonSelector = "#savePaginaButton";
	var galeriaSaveButtonSelector = "#saveGaleriaButton";
	var fotoSaveButtonSelector = "#saveFotoButton";
	
	var contentSelector = ".main .content";
	var infoMenuItemSelector = "#cssmenu #info";
	
	var buttonPaginas = $(paginasSelector);
	var buttonGalerias = $(galeriasSelector);
	var buttonFotos = $(fotosSelector);
	
	var content = $(contentSelector);
	var fileItemEdit = $(fileItemEditSelector);
	var infoMenuItem = $(infoMenuItemSelector);
	
	var list;
	var editor;
	var viewer;
	
	buttonPaginas.click( function() {
		clearContent();
		$(this).parent().addClass("active");
		openPaginas();
	});
	
	buttonGalerias.click( function() {
		clearContent();
		$(this).parent().addClass("active");
		openGalerias();
	});
	
	buttonFotos.click( function() {
		clearContent();
		$(this).parent().addClass("active");
		openFotos(null);
	});
	
	function openPaginas() {
		content.empty();
		paginasList = $("<div>").addClass("items_list grid_5 container alpha");
		paginasEditor = $("<div>").addClass("editor grid_10 container omega");
		content.append(paginasList);
		content.append(paginasEditor);
		$.ajax({
			url: '/paginas',
			data: {},
			dataType: 'html',
			complete : null,
			success: function(html){
				successOpenPaginas(html,paginasList);
				}
		});
		list = paginasList;
		editor = paginasEditor;
	}
	
	function openFotos($dir) {
		content.empty();
		fotosList = $("<div>").addClass("items_list grid_6 alpha container");
		fotosEditor = $("<div>").addClass("editor editor_photos grid_9 omega container");
		content.append(fotosList);
		content.append(fotosEditor);
		if ($dir !== null) {
			$dir = "?path="+$dir;
		} else {
			$dir = "";
		}
		action = '/fotos/listDirectory' + $dir;
		$.ajax({
			url: action,
			data: {},
			dataType: 'html',
			complete: null,
			success: function(html) { 
				successOpenFotos(html,fotosList,$dir); 
			}
		});
		list = fotosList;
		editor = fotosEditor;
	}
	
	function openGalerias() {
		content.empty();
		galeriasList = $("<div>").addClass("items_list grid_5 alpha container");
		galeriasEditor = $("<div>").addClass("editor editor_gallery grid_10 omega container");
		content.append(galeriasList);
		content.append(galeriasEditor);
		$.ajax({
			url: '/galerias/adminList',
			data: {},
			dataType: 'html',
			complete: null,
			success: function(html) { 
				successOpenGalerias(html,galeriasList) 
			}
		});
		list = galeriasList;
		editor = galeriasEditor;
	}
	
	function successOpenPaginas(content, container){
		container.append(content);
		paginaEditButton = $(paginaItemEditSelector);
		paginaEditButton.click(function () {
			openPagina("edit/" + $(this).attr("pagina_id"));
		});
		
		paginaDeleteButton = $(paginaItemDeleteSelector);
		paginaDeleteButton.click(function() {
			var answer = confirm('Seguro quiere borrarlo?');
		    if (answer){
		    	paginaId = $(this).attr("pagina_id");
		    	paginaRow = $(this).parent().parent();
		    	deletePagina(paginaId,paginaRow);
		    }
		    else{
		    	e.preventDefault();      
		    }
		});
		
		$paginaNewButton = $(paginaNewButtonSelector);
		$paginaNewButton.click(function () {
			openPagina("add");
		});
		
		paginaUpButton = $(paginaUpSelector);
		paginaUpButton.click(function(){
			setPaginaOrderUp($(this).parent().parent());
		});
		
		paginaDownButton = $(paginaDownSelector);
		paginaDownButton.click(function(){
			setPaginaOrderDown($(this).parent().parent());
		});
	}
	
	function successOpenFotos(content, container) {
		container.append(content);
		
		fileItemEdit = $(fileItemEditSelector);
		fileItemEdit.click(function() {
			currentPath = $(this).parent().parent().parent().parent().attr('path');
			fileName = $(this).parent().parent().attr("fileName");
			action = "edit/" + $(this).attr("foto_id");
			openFoto(currentPath,fileName,action);
		});
		
		fileItemDelete = $(fileItemDeleteSelector);
		fileItemDelete.click(function() {
			if (!$(this).hasClass("file_not_included")){
				var answer = confirm('Seguro quiere borrarlo?');
				if (answer){
					fotoId = $(this).attr("foto_id");
					currentPath = $(this).parent().parent().parent().parent().attr('path');
					deleteFoto(fotoId,currentPath);
				}
				else{
					e.preventDefault();      
				}
			}
		});
		
		fileItemAdd = $(fileItemAddSelector);
		fileItemAdd.click(function() {
			currentPath = $(this).parent().parent().parent().parent().attr('path');
			fileName = $(this).parent().parent().attr("fileName");
			action = "add/";			
			openFoto(currentPath,fileName,action);
		});

		dirItemView = $(dirItemViewSelector);
		dirItemView.click(function() {
			openFotos($(this).attr("dirName"))
		});
	}
	
	function successOpenGalerias(content, container){
		container.append(content);
		galeriaEditButton = $(galeriaItemEditSelector);
		galeriaEditButton.click(function() {
			action = "edit/" + $(this).attr("galeria_id");
			openGaleria(action);
		});
		
		galeriaDeleteButton = $(galeriaItemDeleteSelector);
		galeriaDeleteButton.click(function() {
			var answer = confirm('Seguro quiere borrarlo?');
		    if (answer){
		    	galeriaId = $(this).attr("galeria_id");
		    	galeriaRow = $(this).parent().parent();
		    	deleteGaleria(galeriaId,galeriaRow);
		    } else {
		    	e.preventDefault();      
		    }
		});
		
		galeriaNewButton = $(galeriaNewButtonSelector);
		galeriaNewButton.click(function(){
			openGaleria("add");
		});
		
		galeriaUpButton = $(galeriaUpSelector);
		galeriaUpButton.click(function(){
			setGaleriaOrderUp($(this).parent().parent());
		});
		
		galeriaDownButton = $(galeriaDownSelector);
		galeriaDownButton.click(function(){
			setGaleriaOrderDown($(this).parent().parent());
		});
	}
	
	function setGaleriaOrderUp($row){
		if ($row.index() !== 0){
			$exchangeRow = $row.parent().children().eq($row.index() - 1);
			setGaleriaOrder($row, $exchangeRow);
		}
	}
	
	function setGaleriaOrderDown($row){
		if ($row.index() !== $row.parent().children().length-1){
			$exchangeRow = $row.parent().children().eq($row.index() + 1);
			setGaleriaOrder($exchangeRow,$row);
		}
	}
	
	function setGaleriaOrder($row1,$row2){
		$galeria_id_1 = $row1.attr("galeria_id");
		$galeria_id_2 = $row2.attr("galeria_id");
		$galeria_order_1 = $row1.attr("galeria_order");
		$galeria_order_2 = $row2.attr("galeria_order");
		actionUrl = '/galerias/edit_order/' + $galeria_id_1 + "/" + $galeria_id_2 + "/" + $galeria_order_1 + "/" + $galeria_order_2;
		$.ajax({
			type : 'POST',
			url: actionUrl,
			data: {},
			dataType: 'html',
			complete: null,
			success: function(html){
				successGaleriaOrder(html, $row1, $row2);
			}
		});
	}
	
	function successGaleriaOrder(contentReceived, $row1, $row2){
		$galeria_order_1 = $row1.attr("galeria_order");
		$galeria_order_2 = $row2.attr("galeria_order");
		$row1.attr("galeria_order", $galeria_order_2);
		$row2.attr("galeria_order", $galeria_order_1);
		if ($galeria_order_1 < $galeria_order_2){
			$row1.before($row2);
		} else {
			$row2.before($row1);
		}
	}
	
	function setPaginaOrderUp($row){
		if ($row.index() !== 0){
			$exchangeRow = $row.parent().children().eq($row.index() - 1);
			setPaginaOrder($row, $exchangeRow);
		}
	}
	
	function setPaginaOrderDown($row){
		if ($row.index() !== $row.parent().children().length-1){
			$exchangeRow = $row.parent().children().eq($row.index() + 1);
			setPaginaOrder($exchangeRow,$row);
		}
	}
	
	function setPaginaOrder($row1,$row2){
		$pagina_id_1 = $row1.attr("pagina_id");
		$pagina_id_2 = $row2.attr("pagina_id");
		$pagina_order_1 = $row1.attr("pagina_order");
		$pagina_order_2 = $row2.attr("pagina_order");
		actionUrl = '/paginas/edit_order/' + $pagina_id_1 + "/" + $pagina_id_2 + "/" + $pagina_order_1 + "/" + $pagina_order_2;
		$.ajax({
			type : 'POST',
			url: actionUrl,
			data: {},
			dataType: 'html',
			complete: null,
			success: function(html){
				successPaginaOrder(html, $row1, $row2);
			}
		});
	}
	
	function successPaginaOrder(contentReceived, $row1, $row2){
		$pagina_order_1 = $row1.attr("pagina_order");
		$pagina_order_2 = $row2.attr("pagina_order");
		$row1.attr("pagina_order", $pagina_order_2);
		$row2.attr("pagina_order", $pagina_order_1);
		if ($pagina_order_1 < $pagina_order_2){
			$row1.before($row2);
		} else {
			$row2.before($row1);
		}
	}
	
	function openPagina(action){
		$.ajax({
			url: '/paginas/' + action,
			data: {},
			dataType: 'html',
			complete: null,
			success: function(html) {
				successOpenPagina(html,action);
			}
		});
	}
	
	function openFoto(path,fileName,action){
		console.log("Opening foto:  path:" + path);
		console.log("filename::" + fileName);
		console.log("action:" + action);
		$.ajax({
			url: '/fotos/' + action ,
			data: { 'path': path, 'fileName' : fileName },
			dataType: 'html',
			complete: null,
			success: function(html) {
				successOpenFoto(html, action, path, fileName);
			}
		});
	}
	
	function openGaleria(action){
		console.log("Opening galeria, action: " + action);
		$.ajax({
			url: '/galerias/' + action,
			data: {},
			dataType: 'html',
			complete: null,
			success: function(html) {
				successOpenGaleria(html,action);
			}
		})
	}
	
	function successOpenPagina(contentReceived, action){
		editor.empty();
		editor.append(contentReceived);
		$commitButton = editor.find(paginaSaveButtonSelector);
		$commitButton.click( function() {
			if (!$commitButton.hasClass("disabled")){
				title = $("form#paginaForm input#title").val();
				texto = $("form#paginaForm textarea#texto").val();
				folder = $('#galleryForm #folder').val();
				answer = $.ajax({
					type: 'POST',
					url: '/paginas/' + action,
					data: { 
						'title' : title,
						'texto' : texto
						},
					dataType: 'html',
					complete: null,
					success: function(html,textStatus,xhr) {
						successSavePagina($commitButton,action,html);
					}
				});
			}
		});
	}
	
	function successOpenFoto(contentReceived, action, path, fileName){
		editor.empty();
		editor.append(contentReceived);
		$commitButton = editor.find(fotoSaveButtonSelector);
		$commitButton.click( function() {
			if (!$commitButton.hasClass("disabled")){
				title = $('#createFotoForm #title').val();
				galeria_id = $('#createFotoForm #galeria_id').val();
				description = $('#createFotoForm #description').val();
				answer = $.ajax({
					type: 'POST',
					url: '/fotos/' + action,
					data: { 
						'title' : title,
						'galeria_id' : galeria_id,
						'description' : description,
						'path' : path,
						'filename' : fileName
						},
					dataType: 'html',
					complete: null,
					success: function(html){
						successSaveFoto($commitButton, action, html, path, fileName);
					}
				});
			}
		});
	} 
	
	function successOpenGaleria(contentReceived,action){
		editor.empty();
		editor.append(contentReceived);
		$commitButton = editor.find(galeriaSaveButtonSelector);
		$commitButton.click( function() {
			if (!$commitButton.hasClass("disabled")){
				title = $('#galleryForm #title').val();
				description = $('#galleryForm #description').val();
				folder = $('#galleryForm #folder').val();
				answer = $.ajax({
					type: 'POST',
					url: '/galerias/' + action,
					data: { 
						'title' : title,
						'description' : description,
						'folder' : folder
						},
					dataType: 'html',
					complete: null,
					success: function(html,textStatus,xhr) {
						successSaveGaleria($commitButton, action, html);
					}
				});
			}
		});
		
		fotoUpButton = $(fotoUpSelector);
		fotoUpButton.click(function(){
			setFotoOrderUp($(this).parent().parent());
		});
		
		fotoDownButton = $(fotoDownSelector);
		fotoDownButton.click(function(){
			setFotoOrderDown($(this).parent().parent());
		});
	}
	
	function setFotoOrderUp($row){
		if ($row.index() !== 0){
			$exchangeRow = $row.parent().children().eq($row.index() - 1);
			setFotoOrder($row, $exchangeRow);
		}
	}
	
	function setFotoOrderDown($row){
		if ($row.index() !== $row.parent().children().length-1){
			$exchangeRow = $row.parent().children().eq($row.index() + 1);
			setFotoOrder($exchangeRow,$row);
		}
	}
	
	function setFotoOrder($row1,$row2){
		$foto_id_1 = $row1.attr("foto_id");
		$foto_id_2 = $row2.attr("foto_id");
		$foto_order_1 = $row1.attr("foto_order");
		$foto_order_2 = $row2.attr("foto_order");
		actionUrl = '/fotos/edit_order/' + $foto_id_1 + "/" + $foto_id_2 + "/" + $foto_order_1 + "/" + $foto_order_2;
		$.ajax({
			type : 'POST',
			url: actionUrl,
			data: {},
			dataType: 'html',
			complete: null,
			success: function(html){
				successFotoOrder(html, $row1, $row2);
			}
		});
	}
	
	function successFotoOrder(contentReceived, $row1, $row2){
		$foto_order_1 = $row1.attr("foto_order");
		$foto_order_2 = $row2.attr("foto_order");
		$row1.attr("foto_order", $foto_order_2);
		$row2.attr("foto_order", $foto_order_1);
		if ($foto_order_1 < $foto_order_2){
			$row1.before($row2);
		} else {
			$row2.before($row1);
		}
	}
	
	function successSavePagina(newCommitButton, action, contentReceived){
		newCommitButton.addClass("disabled");
		if (action === "add"){
			openPaginas();
			$html = $(contentReceived);
			actionString = "edit/" + $html.find("table").attr("pagina_id");
			openPagina(actionString);
		} else {
			openPaginas();
			openPagina(action);
		}
		showInfo("Pagina guardada");
	}
	
	function successSaveFoto(newCommitButton, action, contentReceived, path, fileName){
		newCommitButton.addClass("disabled");
		if (action === "add/"){
			openFotos(path);
			$html = $(contentReceived);
			actionString = "edit/" + $html.find("table").attr("foto_id");
			openFoto(path, fileName, actionString);
		} else {
			openFotos(path);
			openFoto(path, fileName, action);
		}
		showInfo("Foto guardada");
	}
	
	function successSaveGaleria(newCommitButton, action, contentReceived){
		newCommitButton.addClass("disabled");
		if (action === "add"){
			openGalerias();
			$html = $(contentReceived);
			actionString = "edit/" + $html.find("table").attr("galeria_id");
			openGaleria(actionString);
		} else {
			openGalerias();
			openGaleria(action);
		}
		showInfo("Galería guardada");
	}	

	function deletePagina(paginaId, paginaRow){
		action = "delete/" + paginaId;
		$.ajax({
			type : 'POST',
			url: '/paginas/' + action,
			data: {},
			dataType: 'html',
			complete: null,
			success: function(html) {
				paginaRow.remove();
				showInfo("Página borrada.")
			}
		});
	}
	
	function deleteFoto(fotoId, path){
		action = "delete/" + fotoId;
		$.ajax({
			type : 'POST',
			url: '/fotos/' + action,
			data: {},
			dataType: 'html',
			complete: null,
			success: function(html) {
				openFotos(path);
				showInfo("Foto desactivada.")
			}
		});
	}
	
	function deleteGaleria(galeriaId, galeriaRow){
		action = "delete/" + galeriaId;
		$.ajax({
			type : 'POST',
			url: '/galerias/' + action,
			data: {},
			dataType: 'html',
			complete: null,
			success: function(html) {
				galeriaRow.remove();
				showInfo("Galería borrada.")
			}
		});
	}
	
	function showInfo(info) {
		infoContent = $(infoMenuItemSelector + " a");
		infoContent.empty();
		infoContent.append(info);
		infoMenuItem.slideDown().delay(2000).slideUp();		
	}
	
	function clearContent() {
		content.empty();
		$('#cssmenu li').removeClass('active');
	}
	
});