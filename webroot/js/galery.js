$(function() {
	var currentPage = 0;
	var buttonPrevSelector = "#content #content-left .pagination .direction-buttons.prev";
	var buttonNextSelector = "#content #content-left .pagination .direction-buttons.next";
	var directionButtonsSelector = "#content #content-left .pagination .direction-buttons";
	var thumbnailPagesSelector = "#content #content-left ul.thumbnails";
	var thumbnailsSelector = "#content #content-left ul.thumbnails li";
	var numberButtonsSelector = "#content #content-left .pagination .number";
	var backImageSelector = "#content #content-right .slideshow.back img";
	var backImageHolderSelector = "#content #content-right .slideshow.back";
	var frontImageSelector = "#content #content-right .slideshow.front img";
	var frontImageHolderSelector = "#content #content-right .slideshow.front";
	
	var thumbnails = $(thumbnailsSelector);
	var numberButtons = $(numberButtonsSelector);
	var thumbnailPages = $(thumbnailPagesSelector);
	var directionButtons = $(directionButtonsSelector);
	var buttonNext = $(buttonNextSelector);
	var buttonPrev = $(buttonPrevSelector);
	var backImage = $(backImageSelector);
	var frontImage = $(frontImageSelector);
	var backImageHolder = $(backImageHolderSelector);
	var frontImageHolder = $(frontImageHolderSelector);
	
	thumbnails.click(function () {
		thumbnails.removeClass("selected");
		var picToShow = $(this).attr("target");
		showPicture(picToShow);
		$(this).addClass("selected");
	});
	
	numberButtons.click(function() {
		var selectedPage = $(this).attr("title");
		selectPage(selectedPage);
	});
	
	function showPicture($picture){
		backImage.attr("src",$picture);
		backImageHolder.addClass("front");
		frontImageHolder.removeClass("front");
		frontImageHolder.addClass("back");
		backImageHolder.removeClass("back");
		backImageHolder = $(backImageHolderSelector);
		frontImageHolder = $(frontImageHolderSelector);
		backImage = $(backImageSelector);
	}
	
	function selectPage($page) {
		numberButtons.removeClass("current");
		thumbnailPages.removeClass("visible");
		thumbnails.removeClass("selected");
		
		thumbnailPages.each(function(index){
			if (index == $page) {
				$(this).addClass("visible");
				$($(this).find("li")[0]).addClass("selected");
				var photoToShow = $($(this).find("li")[0]).attr("target");
				showPicture(photoToShow);
			}
		});
		
		numberButtons.each(function(index) {
			var title = $(this).attr("title");
			if (title == $page){
				$(this).addClass("current");
			}
		});
		currentPage = $page;
		directionButtons.removeClass("inactive-link")
		if (currentPage == 0) {
			buttonPrev.addClass("inactive-link");
		}
		if (currentPage == 3) {
			buttonNext.addClass("inactive-link");
		}
	}
	
	directionButtons.click(function() {
		if ( $(this).hasClass('prev') ){
			selectPage(currentPage - 1);
		}
		if ( $(this).hasClass('next') ){
			selectPage(currentPage + 1);
		}		
	});
});