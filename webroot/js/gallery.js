$(function() {
	var buttonLeftSelector = ".gallery_control.left";
	var buttonRightSelector = ".gallery_control.right";
	var galleryItemSelector = ".gallery_item";
	var currImageSelector = ".gallery .gallery_item#c";
	var previousImagesSelector = ".gallery .gallery_item.p";
	var nextImagesSelector = ".gallery .gallery_item.n";
	var orderTag = "orderNumber";
	var gallerySelector = ".gallery";
	const PREVIOUS_IMAGE_CLASS = "p";
	const NEXT_IMAGE_CLASS = "n";
	const NEW_ITEM_URL = "/fotos/getGalleryItem/";
	
	var addingImage = false;
	
	$buttonLeft = $(buttonLeftSelector);
	$buttonRight = $(buttonRightSelector);
	$gallery = $(gallerySelector);
	
	var galleryLength = $gallery.attr("gallery_length");
	
	$buttonRight.click( function (){
		moveToNext();		
	} );
	
	function moveToNext() {
		$currentImage = $(currImageSelector);
		$nextImage = $currentImage.next(galleryItemSelector);
		
		if ($nextImage.exists()) {
			
			$currentImage.removeAttr("id");
			$currentImage.addClass(PREVIOUS_IMAGE_CLASS);
			$nextImage.removeClass(NEXT_IMAGE_CLASS);
			$nextImage.attr("id","c");
			addNextImage($currentImage);
		}
	}
	
	$buttonLeft.click( function(){
		moveToPrevious();
	} );
	
	function moveToPrevious() {
		$currentImage = $(currImageSelector);
		$previousImage = $currentImage.prev(galleryItemSelector);
		if ($previousImage.exists()){
			$currentImage.removeAttr("id");
			$currentImage.addClass(NEXT_IMAGE_CLASS);
			$previousImage.removeClass(PREVIOUS_IMAGE_CLASS);
			$previousImage.attr("id","c");
			addPreviousImage($currentImage);
		}
	}
	
	function addPreviousImage($currentImage) {
		currentImageIndex = $currentImage.index();
		firstImageGalleryIndex = $(galleryItemSelector).first().attr(orderTag);
		if (!addingImage && currentImageIndex < 3 && firstImageGalleryIndex > 0){
			newIndex = firstImageGalleryIndex - 1;
			addingImage = true;
			getNewGalleryItem(newIndex,PREVIOUS_IMAGE_CLASS);
		}
	}
	
	function addNextImage($currentImage) {
		lastImageGalleryIndex = parseInt( $(galleryItemSelector).last().attr(orderTag) );
		currentToEnd = $(galleryItemSelector).last().index() - $currentImage.index();
		if (!addingImage && currentToEnd < 3 && ((lastImageGalleryIndex + 1) < galleryLength)){
			newIndex = lastImageGalleryIndex + 1;
			addingImage = true;
			getNewGalleryItem(newIndex,NEXT_IMAGE_CLASS);
		}
	}
	
	function getNewGalleryItem ($orderNumber,$newClass) {
		$galleryId = $gallery.attr("gallery_id");
		$url = NEW_ITEM_URL + $galleryId + "/" + $orderNumber;
		$.ajax({
			type:'GET',
			url: $url,
			dataType: 'html',
			data: {},
			success: function(html) {
				$result = $(html);
				$result.addClass($newClass);
				if ($newClass == PREVIOUS_IMAGE_CLASS){
					$gallery.prepend($result);
				} else {
					$gallery.append($result);
				}
				addingImage = false;
			}
		});
	}
	
	$(document).keydown(function(e) {
	    if (e.which == 39) {
	    	moveToNext();
	    }
	    if (e.which == 37) {
	    	moveToPrevious();
	    }
	    e.preventDefault();
	});
});