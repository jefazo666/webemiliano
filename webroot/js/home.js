$(function() {
	var contentSelector = "#content";
	var content = $(contentSelector);
	var currentPage = 1;
	
	var fotoSelector = "a div.imageHolder img";
	var fotoVerticalSelector = "div.fotoVertical";
	var fotoHorizontalSelector = "div.fotoHorizontal";
	
	var lastPieceSelector = "div.piece:last";
	
	var requestingNewPiece = false;
	
	
	function addFotosRow(){
		actionUrl = "/pieces?page=" + (currentPage + 1); 
		$.ajax({
			type:'GET',
			url: actionUrl,
			dataType: 'html',
			success: function(html) {
				$result = $(html);
				content.append($result);
				var $images = $result.find(fotoSelector);
				$images.each(function(){
					fadeImagesIn($(this));
				});
				$images.on('load',function(){
					calculateImageExpansion($(this));
				});
				currentPage++;
				requestingNewPiece = false;
				
			}
		});
	}
	
	$(window).scroll(function() {
		if (!requestingNewPiece){
		    if( ($(window).scrollTop() + 300 ) > $(document).height() - $(window).height() ) {
		    	requestingNewPiece = true;
		    	addFotosRow();
		    }
		}
	});
	
	function calculateImageExpansion($img){
		var imgWidth = $img.width();
		var imgHeight = $img.height();
		var holderWidth = $img.parent().parent().parent().width();
		var holderHeight = $img.parent().parent().parent().height();
		imgRatio = imgWidth / imgHeight;
		holdRatio = holderWidth / holderHeight;
		if (imgRatio >= holdRatio) {
			$img.addClass("fillheight");
		} else {
			$img.addClass("fillwidth");
		}
	}
	
	function fadeImagesIn($img){
		var tmpImg = new Image();
		$img.css({ opacity: 0 });
		tmpImg.src = $img.attr('src');
		var loadDelay = Math.floor((Math.random() * 1000));
		$img.delay(loadDelay).fadeTo(600,1);
	}
	
	$(document).ready(function() {
		$(fotoSelector).on('load',function(){
			calculateImageExpansion($(this));
			fadeImagesIn($(this));
		});
	});
	
});