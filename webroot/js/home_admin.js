$(function() {
	const ADD_ROW_URL = '/pages/addPhotoRow';
	var contentSelector = "#content";
	var content = $(contentSelector);
	
	var lastPieceSelector = "div.piece:last";
	
	var placeHolderSelector = "div#layoutPlaceholder ";
	var fotoSelector = "a div.imageHolder img";
	var imageBrowserID = 'imageBrowser';
	var imageBrowserHeaderId = 'imageBrowserHeaderId';
	var imageListID = 'imageList';
	var imageBrowserPhotosListId = 'imageBrowserPhotosListId';
	var imagePlaceHolderSelector = 'div.imageHolder.imageAdd';
	
	var layoutBrowserId = 'layoutBrowser';
	var layoutListId = 'layoutList';
	
	var galleryButtonClass = 'galleryButton';
	var browserImageClass = 'browserImage';
	var galleryButtonSelector = 'button.' + galleryButtonClass;
	
	var galerias;
	var layouts;
	var fotosCurrentGaleria;
	var fotosNoGaleria;
	var homePieces = [];
	var selectedPiece;
	
	$placeHolder = $(placeHolderSelector);
	
	$selectedImagePlaceHolder = null;
	
	$imageBrowserGalleriesList = $('<ul/>');
	
	$imageBrowserPhotosList = $('<ul/>', {
		'id':imageBrowserPhotosListId
	});
	
	$imageBrowser = $('<div/>',{
		'id':imageBrowserID
	})
	.append($('<div/>', { 'id':imageBrowserHeaderId})
			.append($('<div/>', {'class':'title', 'html':'Galerías'}))
			.append($('<div/>',{'class':'list'}).append($imageBrowserGalleriesList))
		)
	.append($('<div/>', { 'id':imageListID})
			.append($imageBrowserPhotosList)
		);
	
	$layoutList = $('#' + layoutListId);
	$layoutBrowser = $('#' + layoutBrowserId);
	
	$placeHolder.click( function (){
		$('html, body').animate({
            scrollTop: $(this).offset().top + 'px'
        }, 'fast');
		openLayoutBrowser();
	});
	
	function getImageBrowserData(){
		downloadGalerias();
	}
	
	function getLayoutBrowserData(){
		downloadLayouts();
	}
	
	function getPiecesData(){
		downloadPieces();
	}
	
	function getNoGaleriaFotos(){
		actionUrl =  '/fotos/no_galeria.json';
		$.ajax({
			url: actionUrl,
			success: function(json){
				fotosNoGaleria = json.fotos;
			}
		})
	}
	
	function openLayoutBrowser(){
		$layoutBrowser.addClass("open");
	}
	
	function closeLayoutBrowser(){
		$layoutBrowser.removeClass("open");
	}
	
	function openImageBrowser(){
		$('body').append($imageBrowser);
	}
	
	function downloadGaleria(galleryId){
		$.ajax({
			url: '/fotos/gallery/' + galleryId + '.json',
			success: function (json){
				fotosCurrentGaleria = json.fotos;
				fillCurrentGaleria();
			}
		});
	}
	
	function downloadPieces(){
		actionUrl = "/pieces/home_edit.json";
		$.ajax({
			url: actionUrl,
			success: function(json){
				$.each(json.pieces, function(i,val) {
					homePieces.push(val);
				})
				console.log("Added " + homePieces.length + " pieces");
				bindAddedPieces();
			}
		})
	}
	
	function bindAddedPieces(){
		$(".imageHolder.imageAdd").click(function (){
			pieceId = $(this).find('img').attr('piece_id');
			openImageBrowser();
			pointingPiece = null;
			$.each(homePieces, function(i,val) {
				if (val.id == pieceId) {
					pointingPiece = val;
				}
			});
			if (pointingPiece != null){
				selectImageHolder($(this),pointingPiece);
			}
		});
	}
	
	function fillCurrentGaleria(){
		console.log("Filling gallery");
		$imageBrowserPhotosList.empty();
		$.each(fotosCurrentGaleria,function(i,item){
			$newImage = $("<img/>").attr("src","/galleryPictures/" + item.path + item.filename).addClass(browserImageClass);
			$newImageHolder = $("<li/>");
			$newImageHolder.append($newImage);
			$imageBrowserPhotosList.append($newImageHolder);
			$newImage.click(function(){
				attachImageToImagePlaceHolder(item);
			});
		});
		console.log("Filled gallery");
	}
	
	function attachImageToImagePlaceHolder(jsonFoto) {
		$imageObject = $selectedImagePlaceHolder.find('img');
		$imageObject.attr('src',"/galleryPictures/" + jsonFoto.path + jsonFoto.filename);
		$imageObject.attr('foto_id',jsonFoto.id);
		$selectedImagePlaceHolder.addClass('imageAdded');
		fotoOrder = $imageObject.attr('foto_order');
		fotoPieceId = $imageObject.attr('piece_id');
		calculateImageExpansion($imageObject);
		saveFotoChanges(jsonFoto.id,fotoPieceId,fotoOrder);
	}
	
	function saveFotoChanges(fotoId,fotoPieceId,fotoOrder){
		actionUrl = '/fotos/addToPiece/' + fotoId +".json";
		console.log("actionUrl");
		$.ajax({
			url: actionUrl,
			type: 'POST',
			data: { 'piece_id': fotoPieceId ,
					'piece_order' : fotoOrder
			},
			success: function(json){
				console.log("success updating foto order");
			},
			error: function(json){
				console.log("error on updating foto order");
			}
		})
	}
	
	function checkPieceReady(selectedPiece) {
		if (selectedPiece.fotos.length == selectedPiece.home_layout.count){
			console.log("piece " + selectedPiece.items_order + " is ready");
		} else {
			console.log("piece " + selectedPiece.items_order + " is not ready");
		}
	}
	
	
	function downloadGalerias(){
		$.ajax({
			url: '/galerias/listAll.json',
			complete: function(json){
			},
			success: function (json){
				galerias = json.galerias;
				fillGalerias();
			}
		});
	}
	
	function fillGalerias(){
		$imageBrowserGalleriesList.empty();
		$.each(galerias,function(i,item){
			var itemClass = galleryButtonClass;
			if (i == 0){
				itemClass = galleryButtonClass + " selected";
				fotosCurrentGaleria = item.fotos;
				fillCurrentGaleria();
			}
			$newGallery = $('<button/>',{
				'html':item.title,
				'class': itemClass,
				'galleryId' : item.id
				});
			$newGallery.click(function() {
				$(galleryButtonSelector).removeClass('selected');
				fotosCurrentGaleria = item.fotos;
				fillCurrentGaleria();
				$(this).addClass('selected');
			});
			$imageBrowserGalleriesList.append($newGallery);
		});
		$newGallery = $('<button/>',{
			'html': 'Fotos Sin Galeria',
			'class': galleryButtonClass
			});
		$newGallery.click(function() {
			$(galleryButtonSelector).removeClass('selected');
			fotosCurrentGaleria = fotosNoGaleria;
			fillCurrentGaleria();
			$(this).addClass('selected');
		});
		$imageBrowserGalleriesList.append($newGallery);
	}
	
	function downloadLayouts(){
		$.ajax({
			url: '/homeLayouts.json',
			complete: function(json){
			},
			success: function (json) {
				layouts = json.homeLayouts;
				fillLayouts();
			}
		});
	}
	
	function fillLayouts(){
		$layoutList.empty();
		$.each(layouts, function(i,item){
			$newItem = $('<li/>').addClass('layout_image');
			$newImage = $('<img/>').attr('src','/img/admin/home/layouts/' + item.preview);
			$newItem.append($newImage);
			$layoutList.append($newItem);
			$newItem.click(function(){
				addLayout(item);
			});
		});
	}
	
	function addLayout(layoutJson){
		accessUrl = '/homeLayouts/getToEdit/' + layoutJson.id + ".json";
		$.ajax({
			url: accessUrl,
			success: function(json){
				addNewPiece(json.homeLayout);
				closeLayoutBrowser();
			}
		});		
	}
	
	function selectImageHolder($element,piece){
		$(imagePlaceHolderSelector).removeClass("selected");
		$element.addClass("selected");
		$selectedImagePlaceHolder = $element;
		selectedPiece = piece;
	}
	
	function addNewPiece (homeLayout){
		$placeToAdd = $placeHolder.parent().parent().parent();
		$itemOrder = null
		if (homePieces.length == 0) {
			$itemOrder = 1;
		} else {
			$itemOrder = homePieces[homePieces.length-1].items_order + 1;
		}
		accessUrl = "/pieces/add.json";
		$.ajax({
			url: accessUrl,
			type : 'POST',
			data : { '_method' : 'POST',
					 'home_layout_id' : homeLayout.id,
					 'items_order'	: $itemOrder,
					 'fotos' : []
			},
			success : function(json){
				jsonPiece = JSON.parse(json);
				successSaveNewPiece($placeToAdd,homeLayout.code,jsonPiece.piece);
			},
			error: function(){
				console.log("error on savign");
			}			
		});
	}
	
	function successSaveNewPiece($pieceDestination, pieceCode,savedPieceJson){
		
		$pieceDestination.before(pieceCode);
		homePieces.push(savedPieceJson);
		$addedPiece = $pieceDestination.prev();
		$addedPiece.find(imagePlaceHolderSelector + " img").attr('piece_id',savedPieceJson.id);
		$addedPiece.attr('piece_id',savedPieceJson.id);
		$addedPiece.find(imagePlaceHolderSelector).click(function() {
			openImageBrowser();
			selectImageHolder($(this),savedPieceJson);
		});
		addDeleteButton($addedPiece);
	}
	
	function calculateImageExpansion($img){
		var imgWidth = $img.width();
		var imgHeight = $img.height();
		var holderWidth = $img.parent().width();
		var holderHeight = $img.parent().height();
		imgRatio = imgWidth / imgHeight;
		holdRatio = holderWidth / holderHeight;
		$img.removeClass("fillheight");
		$img.removeClass("fillwidth");
		if (imgRatio > holdRatio) {
			$img.addClass("fillheight");
		} else {
			$img.addClass("fillwidth");
		}
	}
	
	function addDeleteButton($piece){
		$buttonContainer = $('<div/>').addClass('buttonContainer');
		$buttonHolder = $('<div/>').addClass('buttonHolder');
		$button = $('<img/>').attr('src','/img/admin/home/delete.png');
		$buttonContainer.append($buttonHolder.append($button));
		$piece.append($buttonContainer);
		$button.click(function(){
			deletePiece($piece);
		})
	}
	
	function deletePiece($piece){
		piece_id = $piece.attr('piece_id');
		actionUrl = '/pieces/delete/' + piece_id + ".json";
		$.ajax({
			url: actionUrl,
			type: 'POST',
			success : function(){
				$piece.remove();
			}
		})
	}
	
	function updateData(){
		getLayoutBrowserData();
		getImageBrowserData();
		getNoGaleriaFotos();
	}
	
	
	$(document).ready(function() {
		updateData();
		getPiecesData();
		$('.piece').each( function(){
			addDeleteButton($(this));
		});		
		$(fotoSelector).on('load',function(){
			calculateImageExpansion($(this));
		});
	});	
});