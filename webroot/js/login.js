$(function() {
	const LOGIN_URL = '/users/login';
	var buttonSelector = "div.login_logo a";
	var squareSelector = "div.login_background";
	var logoSelector = ".logo img"; 
	$logo = $(logoSelector);
	$square = $(squareSelector);
	var userSelector = "form input#email";
	var passSelector = "form input#password";
	var messageSelector = "form .form_message"; 
	
	$formMessage = $(messageSelector);
	$starLink = $(buttonSelector);
	
	function setLoginShadow() {
		$logo.addClass("shadowed");
		$square.addClass("shadowed");
	}
	
	function setRedirect($redirect) {
		$starLink.attr("href",$redirect);
	}
	
	$("form").submit( function () {
		$formMessage.empty();
		login();
		return false;
	});
	
	function stringIsEmpty(str){
		return (str === null || str.match(/^ *$/) !== null);
	}
	
	function login(){
		$user = $(userSelector).val();
		$pass = $(passSelector).val();
		if (!stringIsEmpty($user) && !stringIsEmpty($pass)) {
			$.ajax({
				type: 'POST',
				url: LOGIN_URL,
				data: { 
					'email' : $user,
					'password' : $pass
					},
				dataType: 'html',
				complete: null,
				success: function(html,textStatus,xhr) {
					var result = $.parseJSON(html);
					if (result.status == 'OK'){
						setLoginShadow();
						setRedirect(result.redirect);
					} else {
						showMessage("Nombre de usuario o contraseña incorrectos.","error");
					}
				}
			});
		} else {
			if (stringIsEmpty($user)){
				$userInput = $(userSelector);
				$userInput.val("");
				$userInput.parent().effect("shake");
			}
			if (stringIsEmpty($pass)){
				$passInput = $(passSelector);
				$passInput.val("");
				$passInput.parent().effect("shake");
			}
			if (stringIsEmpty($user) || stringIsEmpty($pass)){
				showMessage("Relleane los campos vacíos","error");
			}
		}
	}
	
	function showMessage(message,messageType){
		$message = $("<div>").addClass(messageType).css("display","none").html(message);
		$formMessage.append($message);
		$message.toggle("slide");
	}
});